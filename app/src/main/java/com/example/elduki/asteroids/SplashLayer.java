package com.example.elduki.asteroids;

import android.view.MotionEvent;

import org.cocos2d.layers.Layer;
import org.cocos2d.nodes.Sprite;

class SplashLayer extends Layer {
    Game game;

    public SplashLayer(Game _game) {
        game = _game;

        Sprite title = Sprite.sprite("title.png");
        title.setScale(8);
        title.setPosition(game.size.width / 2, game.size.height / 2);
        super.addChild(title);

        Sprite start = Sprite.sprite("start.png");
        start.setScale(4);
        start.setPosition(game.size.width / 2, game.size.height / 2 - 150);
        super.addChild(start);

        setIsTouchEnabled(true);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        game.play();
        return true;
    }
}
