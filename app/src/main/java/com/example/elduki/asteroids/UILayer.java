package com.example.elduki.asteroids;

import org.cocos2d.layers.Layer;
import org.cocos2d.nodes.Sprite;

class UILayer extends Layer {
    Game game;
    Sprite buttons = Sprite.sprite("buttons.png");

    public UILayer(Game _game) {
        game = _game;
        buttons.setScale(game.size.width / buttons.getWidth());
        buttons.setOpacity(127);
        buttons.setAnchorPoint(0, 0);
        super.addChild(buttons);
    }
}
