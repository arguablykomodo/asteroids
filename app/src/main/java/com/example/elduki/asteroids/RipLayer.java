package com.example.elduki.asteroids;

import android.view.MotionEvent;

import org.cocos2d.layers.Layer;
import org.cocos2d.nodes.Label;
import org.cocos2d.nodes.Sprite;

class RipLayer extends Layer {
    Game game;

    public RipLayer(Game _game) {
        game = _game;

        Sprite gameOver = Sprite.sprite("gameOver.png");
        gameOver.setScale(8);
        gameOver.setPosition(game.size.width / 2, game.size.height / 2);
        super.addChild(gameOver);

        Label score = Label.label("Score: " + game.score, "Source Code Pro", 40);
        score.setPosition(game.size.width / 2, game.size.height / 2 - 85);
        super.addChild(score);

        Sprite tryAgain = Sprite.sprite("tryAgain.png");
        tryAgain.setScale(4);
        tryAgain.setPosition(game.size.width / 2, game.size.height / 2 - 150);
        super.addChild(tryAgain);

        setIsTouchEnabled(true);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        game.play();
        return true;
    }
}
