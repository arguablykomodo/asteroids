package com.example.elduki.asteroids;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;

import org.cocos2d.layers.Layer;
import org.cocos2d.nodes.Director;
import org.cocos2d.nodes.Sprite;

import java.util.ArrayList;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.exp;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

class PlayerLayer extends Layer {
    Game game;
    Sprite player = Sprite.sprite("player.png");
    ArrayList<Sprite> bullets = new ArrayList<>();
    MediaPlayer shootSound, explosion;
    float px, py;
    float vx = 0;
    float vy = 0;
    float rot = 0;

    public PlayerLayer(Game _game) {
        game = _game;
        player.setScale(4);
        player.setPosition(game.size.width / 2, game.size.height / 2);
        px = game.size.width / 2;
        py = game.size.height / 2;
        shootSound = MediaPlayer.create(Director.sharedDirector().getActivity(), R.raw.shoot);
        explosion = MediaPlayer.create(Director.sharedDirector().getActivity(), R.raw.explode);
        super.addChild(player);
        super.schedule("physics", 1 / 60);
        setIsTouchEnabled(true);
    }

    public void physics(float dt) {
        px = px + vx * dt;
        while (px > game.size.width) px -= game.size.width;
        while (px < 0) px += game.size.width;
        py = py + vy * dt;
        while (py > game.size.height) py -= game.size.height;
        while (py < 0) py += game.size.height;
        vx *= 0.995;
        vy *= 0.995;
        player.setPosition(px, py);

        for (Sprite bullet : bullets) {
            bullet.setPosition(
                bullet.getPositionX() + 10 * (float) cos(-bullet.getRotation() / 180 * PI),
                bullet.getPositionY() + 10 * (float) sin(-bullet.getRotation() / 180 * PI)
            );
            if (
                bullet.getPositionX() > game.size.width ||
                bullet.getPositionX() < 0 ||
                bullet.getPositionY() > game.size.height ||
                bullet.getPositionY() < 0
            ) {
                bullets.remove(bullet);
                super.removeChild(bullet, true);
            }

            for (int i = 0; i < game.asteroidLayer.asteroids.size(); i++) {
                Sprite asteroid = game.asteroidLayer.asteroids.get(i);
                float dx = asteroid.getPositionX() - bullet.getPositionX();
                float dy = asteroid.getPositionY() - bullet.getPositionY();
                double distance = sqrt(dx * dx + dy * dy);
                if (distance < asteroid.getWidth() / 2 * asteroid.getScaleX() + bullet.getHeight() / 2 * bullet.getScaleY()) {
                    explosion.start();
                    bullets.remove(bullet);
                    removeChild(bullet, true);
                    game.asteroidLayer.splitAsteroid(asteroid);
                }
            }
        }

        for (int i = 0; i < game.asteroidLayer.asteroids.size(); i++) {
            Sprite asteroid = game.asteroidLayer.asteroids.get(i);
            float dx = asteroid.getPositionX() - player.getPositionX();
            float dy = asteroid.getPositionY() - player.getPositionY();
            double distance = sqrt(dx * dx + dy * dy);
            if (distance < asteroid.getWidth() / 2 * asteroid.getScaleX() + player.getHeight() / 2 * player.getScaleY()) {
                unschedule("physics");
                game.rip();
            }
        }
    }

    // Super hacky way to ratelimit bullet amount
    int bulletTime = 0;

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        bulletTime = 0;
        return true;
    }

    @Override
    public boolean ccTouchesMoved(MotionEvent event) {
        float y = game.size.height - event.getY();
        float h = game.uiLayer.buttons.getHeight() * game.uiLayer.buttons.getScaleY();

        if (y < h / 2) {
            if (event.getX() < game.size.width / 3) {
                rot += 0.05;
                player.setRotation((float) (-rot * 180 / PI));
            } else if (event.getX() > game.size.width / 3 * 2) {
                rot -= 0.05;
                player.setRotation((float) (-rot * 180 / PI));
            } else {
                vx += 20 * cos(rot);
                vy += 20 * sin(rot);
            }
        } else if (y < h && bulletTime <= 0) {
            shootSound.start();
            Sprite bullet = Sprite.sprite("bullet.png");
            bullet.setScale(4);
            bullet.setPosition(px, py);
            bullet.setRotation((float) (-rot * 180 / PI));
            bullets.add(bullet);
            super.addChild(bullet);
            bulletTime = 16;
        }
        bulletTime--;
        return true;
    }
}
