package com.example.elduki.asteroids;

import android.media.MediaPlayer;

import org.cocos2d.nodes.Director;
import org.cocos2d.nodes.Scene;
import org.cocos2d.nodes.Sprite;
import org.cocos2d.opengl.CCGLSurfaceView;
import org.cocos2d.types.CCSize;

public class Game {
    CCGLSurfaceView view;
    CCSize size;

    PlayerLayer playerLayer;
    AsteroidLayer asteroidLayer;
    UILayer uiLayer;
    int score = 0;

    public Game(CCGLSurfaceView view) {
        this.view = view;
    }

    public void start() {
        MediaPlayer bg = MediaPlayer.create(Director.sharedDirector().getActivity(), R.raw.serpent_genesis);
        bg.setLooping(true);
        bg.setVolume(0.1f, 0.1f);
        bg.start();

        Director.sharedDirector().attachInView(view);
        size = Director.sharedDirector().displaySize();
        Director.sharedDirector().runWithScene(splashScene());
    }

    public void play() {
        score = 0;
        Director.sharedDirector().runWithScene(playScene());
    }

    public void rip() {
        MediaPlayer ripSound = MediaPlayer.create(Director.sharedDirector().getActivity(), R.raw.rip);
        ripSound.start();

        Director.sharedDirector().runWithScene(ripScene());
    }

    Scene splashScene() {
        Scene scene = Scene.node();
        scene.addChild(new SplashLayer(this));
        return scene;
    }

    Scene playScene() {
        Scene scene = Scene.node();
        playerLayer = new PlayerLayer(this);
        scene.addChild(playerLayer);
        asteroidLayer = new AsteroidLayer(this);
        scene.addChild(asteroidLayer);
        uiLayer = new UILayer(this);
        scene.addChild(uiLayer);
        return scene;
    }

    Scene ripScene() {
        Scene scene = Scene.node();
        scene.addChild(new RipLayer(this));
        return scene;
    }
}
