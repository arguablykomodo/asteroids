package com.example.elduki.asteroids;

import android.media.MediaPlayer;

import org.cocos2d.actions.instant.CallFuncN;
import org.cocos2d.actions.interval.MoveTo;
import org.cocos2d.actions.interval.Sequence;
import org.cocos2d.layers.Layer;
import org.cocos2d.nodes.CocosNode;
import org.cocos2d.nodes.Director;
import org.cocos2d.nodes.Sprite;

import java.util.ArrayList;
import java.util.Random;

class AsteroidLayer extends Layer {
    Game game;
    public ArrayList<Sprite> asteroids = new ArrayList<>();
    Random r = new Random();
    float speed = 20;
    float interval = 4;

    Sprite asteroid1 = Sprite.sprite("asteroid1.png");
    Sprite asteroid2 = Sprite.sprite("asteroid2.png");

    public AsteroidLayer(Game _game) {
        game = _game;
        createAsteroid(0);
        schedule("createAsteroid", interval);
    }

    public void createAsteroid(float dt) {
        Sprite asteroid = Sprite.sprite("asteroid2.png");
        asteroid.setScale(4);
        asteroid.setRotation(r.nextFloat() * 360);
        asteroids.add(asteroid);
        addChild(asteroid);

        float x1, y1, x2, y2;
        float random = r.nextFloat();
        if (random < 0.25) {
            x1 = 0 - asteroid.getWidth() * asteroid.getScaleX();
            y1 = r.nextFloat() * game.size.height;
            x2 = game.size.width + asteroid.getWidth() * asteroid.getScaleX();
            y2 = r.nextFloat() * game.size.height;
        } else if (random < 0.5) {
            x1 = game.size.width + asteroid.getWidth() * asteroid.getScaleX();
            y1 = r.nextFloat() * game.size.height;
            x2 = 0 - asteroid.getWidth() * asteroid.getScaleX();
            y2 = r.nextFloat() * game.size.height;
        } else if (random < 0.75) {
            x1 = r.nextFloat() * game.size.width;
            y1 = 0 - asteroid.getHeight() * asteroid.getScaleY();
            x2 = r.nextFloat() * game.size.width;
            y2 = game.size.height + asteroid.getHeight() * asteroid.getScaleY();
        } else {
            x1 = r.nextFloat() * game.size.width;
            y1 = game.size.height + asteroid.getHeight() * asteroid.getScaleY();
            x2 = r.nextFloat() * game.size.width;
            y2 = 0 - asteroid.getHeight() * asteroid.getScaleY();
        }

        asteroid.setPosition(x1, y1);
        asteroid.runAction(Sequence.actions(MoveTo.action(speed, x2, y2), CallFuncN.action(this, "outOfBounds")));
        speed *= 0.99;

        unschedule("createAsteroid");
        schedule("createAsteroid", interval);
    }

    void splitAsteroid(Sprite asteroid) {
        interval *= 0.99;

        float x1 = asteroid.getPositionX();
        float y1 = asteroid.getPositionY();

        int size = 0;
        if (asteroid.displayFrame() == asteroid1.displayFrame()) size = 1;
        else if (asteroid.displayFrame() == asteroid2.displayFrame()) size = 2;

        switch (size) {
            case 0: game.score += 500; break;
            case 1: game.score += 250; break;
            case 2: game.score += 125; break;
        }

        asteroids.remove(asteroid);
        removeChild(asteroid, true);

        if (size == 0) return;

        String filename = "asteroid0.png";
        if (size == 2) filename = "asteroid1.png";

        for (int i = 0; i < 2; i++) {
            Sprite newAsteroid = Sprite.sprite(filename);
            newAsteroid.setRotation(r.nextFloat() * 360);
            newAsteroid.setScale(4);
            asteroids.add(newAsteroid);
            addChild(newAsteroid);

            float x2, y2;
            float random = r.nextFloat();
            if (random < 0.25) {
                x2 = game.size.width + newAsteroid.getWidth() * newAsteroid.getScaleX();
                y2 = r.nextFloat() * game.size.height;
            } else if (random < 0.5) {
                x2 = 0 - newAsteroid.getWidth() * newAsteroid.getScaleX();
                y2 = r.nextFloat() * game.size.height;
            } else if (random < 0.75) {
                x2 = r.nextFloat() * game.size.width;
                y2 = game.size.height + newAsteroid.getHeight() * newAsteroid.getScaleY();
            } else {
                x2 = r.nextFloat() * game.size.width;
                y2 = 0 - newAsteroid.getHeight() * newAsteroid.getScaleY();
            }

            newAsteroid.setPosition(x1, y1);
            newAsteroid.runAction(Sequence.actions(MoveTo.action(speed, x2, y2), CallFuncN.action(this, "outOfBounds")));
        }
    }

    public void outOfBounds(CocosNode asteroid) {
        asteroids.remove(asteroid);
        removeChild(asteroid, true);
    }
}
