package com.example.elduki.asteroids;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.cocos2d.opengl.CCGLSurfaceView;
import org.cocos2d.opengl.Texture2D;
import org.cocos2d.types.CCTexParams;

import javax.microedition.khronos.opengles.GL10;

public class MainActivity extends Activity {
    CCGLSurfaceView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Texture2D.setTexParameters(new CCTexParams(GL10.GL_NEAREST, GL10.GL_NEAREST, GL10.GL_CLAMP_TO_EDGE, GL10.GL_CLAMP_TO_EDGE));
        view = new CCGLSurfaceView(this);
        setContentView(view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Game game = new Game(view);
        game.start();
    }
}
